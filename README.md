## Overview

This repository includes scripts and Dockerfiles that create server pods for the
WebAFS to EOS project. This project aims to provide an easy yet complete way to
deploy php/python websites on OpenShift, using EOS, without the need to interact
with the OpenShift console.

## Building the project

Currently, there are two ways to build the project. In any case, the deployment
of the pods is handled by OpenShift.

### 1. Using Helm and Gitlab Pipeline

Deployment of the docker image on the OpenShift cluster is done via Helm and Gitlab pipeline. This will create the necessary resources on the OpenShift cluster
and deploy the docker images as per the version (base / extra) specified.

### 2. Using YAML templates

OpenShift can consume any of the YAML templates in this repository and automatically create all of the required resources
(Build Configs, Services, Routes, etc) for the project.
This requires a local clone of the YAML template and the installation of [Openshift
Client Tools](https://developers.openshift.com/managing-your-applications/client-tools.html) on your machine.

You can consume a template by running `$ oc new-app -f template/<template_name>`.

In this case, OpenShift handles the building and deployment of your project. The deployment templates are present in deprecated/deploy folder.
