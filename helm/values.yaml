name: webeos-base
namespace: webeos
application_domain: webeos-base.web.cern.ch
replicas: 2

secret:
  name: keytab
  keytabPassword: findmeout
  keytabUser: wwweos

imagestream:
  name: webeos
  repository: gitlab-registry.cern.ch/webservices/webeos/webeos
  pullpolicy: ifNotPresent
  pullsecret_name: registry
  tag_name: base-BRANCH_NAME
  tag_from: gitlab-registry.cern.ch/webservices/webeos/webeos:base-BRANCH_NAME

persistence:
  ## A manually managed Persistent Volume and Claim
  ## Requires persistence.enabled: true
  ## If defined, PVC must be created manually before volume will be bound
  # existingClaim:
  enabled: true
  name: pvc-eos
  storage_class: eos
  storage_provisioner: cern.ch/volume-provisioner-eos
  accessMode: ReadWriteMany
  size: 2Gi

containers:
  eosclient:
    name: eosclient-openshift
    image_name: gitlab-registry.cern.ch/paas-tools/eosclient-openshift:latest
    image_pullpolicy: Always
    env:
    - name: KEYTAB_USER
      valueFrom:
        secretKeyRef:
          name: keytab
          key: keytabuser
    - name: KEYTAB_PWD
      valueFrom:
        secretKeyRef:
          name: keytab
          key: keytabpwd
    livenessProbe:
      exec:
        command:
        - stat
        - /var/run/eosd/credentials
      initialDelaySeconds: 30
    readinessProbe:
      exec:
        command:
        - stat
        - /var/run/eosd/credentials
    resources:
      limits:
        cpu: 1000m
        memory: 512Mi
      requests:
        cpu: 500m
        memory: 128Mi
    securityContext:
      runAsUser: config-uid
      capabilities: {}
      privileged: false
    volumeMounts:
      - mountPath: /var/run/eosd/credentials
        name: eos
        subPath: .eoscredentials

  httpd:
    name: httpd-base
    image_name: webeos:base-BRANCH_NAME
    image_pullpolicy: Always
    command: /httpd.sh
    env:
    - name: NAMESPACE
      valueFrom:
        fieldRef:
          apiVersion: v1
          fieldPath: metadata.namespace
    - name: SERVICE_NAME
      value: webeos-base
    - name: HOSTNAME_FQDN
      value: ""
    - name: APACHE_UID
      value: "config-uid"
    livenessProbe:
      exec:
        command:
        - python
        - liveness.py
        - ;
        - cat
        - /eos/project/w/webservices/wwweos/readiness
      initialDelaySeconds: 5
      timeoutSeconds: 90
    readinessProbe:
      exec:
        command:
        - python
        - liveness.py
        - ;
        - cat
        - /eos/project/w/webservices/wwweos/readiness
    resources:
      limits:
        cpu: 4000m
        memory: 2048Mi
      requests:
        cpu: 1000m
        memory: 1024Mi
    initialDelaySeconds: 4
    timeoutSeconds: 15
    securityContext:
      runAsUser: 0
      capabilities: {}
      privileged: false
    terminationMessagePath: /dev/termination-log
    volumeMounts:
        # Shared mount for communication between all containers
      - name: shared
        mountPath: /var/run/shibboleth
      - name: log-dir
        mountPath: /var/log/httpd
      - name: vhost
        mountPath: /etc/httpd/conf.d/vhost
        readOnly: true
      - name: shibboleth-config
        mountPath: /etc/shibboleth/webservicesDynamic
        readOnly: true
      - name: eos
        mountPath: /eos
      - # Hide eos credentials folder as webeos runs as root. With
        # this mount, the folder won't be accessible from inside
        # the container. This is extremely important so applications
        # running in webeos cannot obtain the Kerberos credentials
        # used to access EOS.
        name: hide-eos-credentials
        mountPath: /eos/.eoscredentials
        readOnly: true

  shibd:
    name: shibd-base
    image_name: webeos:base-BRANCH_NAME
    image_pullpolicy: Always
    command: /shib.sh
    env:
    - name: NAMESPACE
      valueFrom:
        fieldRef:
          apiVersion: v1
          fieldPath: metadata.namespace
    - name: HOSTNAME_FQDN
      value: ""
    livenessProbe:
      exec:
        command:
        - test
        - -S /var/run/shibboleth/shibd.sock
      initialDelaySeconds: 5
      timeoutSeconds: 90
    readinessProbe:
      exec:
        command:
        - test
        - -S /var/run/shibboleth/shibd.sock
    resources:
      limits:
        cpu: 1000m
        memory: 512Mi
      requests:
        cpu: 500m
        memory: 128Mi
    securityContext:
      runAsUser: 0
      capabilities: {}
      privileged: false
    terminationMessagePath: /dev/termination-log
    volumeMounts:
        # Shared mount for communication between all containers
      - name: shared
        mountPath: /var/run/shibboleth
      - name: shibboleth-config
        mountPath: /etc/shibboleth/webservicesDynamic
        readOnly: true

  logstash:
    name: logstash
    image_name: gitlab-registry.cern.ch/webservices/webeos/webeos-logstash:latest
    image_pullpolicy: Always
    livenessProbe:
      exec:
        command:
        - test
        - -e /var/lib/logstash/uuid
      initialDelaySeconds: 5
      timeoutSeconds: 90
    readinessProbe:
      exec:
        command:
        - test
        - -e /var/lib/logstash/uuid
    resources:
      limits:
        cpu: 2000m
        memory: 2048Mi
      requests:
        cpu: 1000m
        memory: 1024Mi
    volumeMounts:
      - name: log-dir
        mountPath: /data

  controller:
    name: config-controller
    image_name: webeos-config-controller:BRANCH_NAME
    image_pullpolicy: Always
    resources:
      limits:
        cpu: 250m
        memory: 512Mi
      requests:
        cpu: 100m
        memory: 256Mi
    terminationMessagePath: /dev/termination-log
    volumeMounts:
      - name: vhost
        mountPath: /config
      - name: shibboleth-config
        mountPath: /shib-config

  dnsPolicy: ClusterFirst
  restartPolicy: Always
  volumes:
    - name: shared
      emptyDir: {}
    - name: shibboleth-config
      emptyDir: {}
    - name: vhost
      emptyDir: {}
    - name: eos
      persistentVolumeClaim:
        claimName: pvc-eos
    - name: hide-eos-credentials
      projected:
        sources: {}
    - name: log-dir
      emptyDir: {}
