kind: DeploymentConfig
apiVersion: apps.openshift.io/v1
metadata:
  labels:
    app: {{ .Values.name | quote }}
  name: {{ .Values.name | quote }}
spec:
  replicas: {{ .Values.replicas | quote }}
  selector:
    app: {{ .Values.name | quote }}
    deploymentconfig: {{ .Values.name | quote }}
  strategy:
    type: Rolling
  template:
    metadata:
      labels:
        app: {{ .Values.name | quote }}
        deploymentconfig: {{ .Values.name | quote }}
    spec:
      containers:
        -
          name: {{ .Values.containers.eosclient.name | quote }}
          image: {{ .Values.containers.eosclient.image_name | quote }}
          imagePullPolicy: {{ .Values.containers.eosclient.image_pullpolicy | quote }}
          env:
{{ toYaml .Values.containers.eosclient.env | indent 12 }}
          livenessProbe:
{{ toYaml .Values.containers.eosclient.livenessProbe | indent 12 }}
          readinessProbe:
{{ toYaml .Values.containers.eosclient.readinessProbe | indent 12 }}
          resources:
{{ toYaml .Values.containers.eosclient.resources | indent 12 }}
          securityContext:
{{ toYaml .Values.containers.eosclient.securityContext | indent 14 }}
          volumeMounts:
{{ toYaml .Values.containers.eosclient.volumeMounts | indent 12 }}

        -
          name: {{ .Values.containers.httpd.name | quote }}
          image: {{ .Values.containers.httpd.image_name | quote }}
          imagePullPolicy: {{ .Values.containers.httpd.image_pullpolicy | quote }}
          command:
            - {{ .Values.containers.httpd.command | quote }}
          env:
{{ toYaml .Values.containers.httpd.env | indent 12 }}
          livenessProbe:
{{ toYaml .Values.containers.httpd.livenessProbe | indent 12 }}
          readinessProbe:
{{ toYaml .Values.containers.httpd.readinessProbe | indent 12 }}
          resources:
{{ toYaml .Values.containers.httpd.resources | indent 12 }}
          initialDelaySeconds: {{ .Values.containers.httpd.initialDelaySeconds | quote }}
          timeoutSeconds: {{ .Values.containers.httpd.timeoutSeconds | quote }}
          securityContext:
{{ toYaml .Values.containers.httpd.securityContext | indent 14 }}
          terminationMessagePath: {{ .Values.containers.httpd.terminationMessagePath | quote }}
          volumeMounts:
{{ toYaml .Values.containers.httpd.volumeMounts | indent 12 }}

        -
          name: {{ .Values.containers.shibd.name | quote }}
          image: {{ .Values.containers.shibd.image_name | quote }}
          imagePullPolicy: {{ .Values.containers.shibd.image_pullpolicy | quote }}
          command:
            - {{ .Values.containers.shibd.command | quote }}
          env:
{{ toYaml .Values.containers.shibd.env | indent 12 }}
          livenessProbe:
{{ toYaml .Values.containers.shibd.livenessProbe | indent 12 }}
          readinessProbe:
{{ toYaml .Values.containers.shibd.readinessProbe | indent 12 }}
          resources:
{{ toYaml .Values.containers.shibd.resources | indent 12 }}
          securityContext:
{{ toYaml .Values.containers.shibd.securityContext | indent 14 }}
          terminationMessagePath: {{ .Values.containers.shibd.terminationMessagePath | quote }}
          volumeMounts:
{{ toYaml .Values.containers.shibd.volumeMounts | indent 12 }}

        -
          name: {{ .Values.containers.logstash.name | quote }}
          image: {{ .Values.containers.logstash.image_name | quote }}
          imagePullPolicy: {{ .Values.containers.logstash.image_pullpolicy | quote }}
          livenessProbe:
{{ toYaml .Values.containers.logstash.livenessProbe | indent 12 }}
          readinessProbe:
{{ toYaml .Values.containers.logstash.readinessProbe | indent 12 }}
          resources:
{{ toYaml .Values.containers.logstash.resources | indent 12 }}
          terminationMessagePath: {{ .Values.containers.controller.terminationMessagePath | quote }}
          terminationMessagePolicy: File
          volumeMounts:
{{ toYaml .Values.containers.logstash.volumeMounts | indent 12 }}

        -
          name: {{ .Values.containers.controller.name | quote }}
          image: {{ .Values.containers.controller.image_name | quote }}
          imagePullPolicy: {{ .Values.containers.controller.image_pullpolicy | quote }}
          resources:
{{ toYaml .Values.containers.controller.resources | indent 12 }}
          volumeMounts:
{{ toYaml .Values.containers.controller.volumeMounts | indent 12 }}

      dnsPolicy: {{ .Values.containers.dnsPolicy | quote }}
      restartPolicy: {{ .Values.containers.restartPolicy | quote }}
      serviceAccount: anyuid
      volumes:
{{ toYaml .Values.containers.volumes | indent 10 }}

  triggers:
    - imageChangeParams:
        automatic: true
        containerNames:
          - {{ .Values.containers.controller.name | quote }}
        from:
          kind: ImageStreamTag
          name: {{ .Values.containers.controller.image_name | quote }}
          namespace: {{ .Values.namespace | quote }}
      type: ImageChange
    - imageChangeParams:
        automatic: true
        containerNames:
          - {{ .Values.containers.httpd.name | quote }}
        from:
          kind: ImageStreamTag
          name: {{ .Values.containers.httpd.image_name | quote }}
          namespace: {{ .Values.namespace | quote }}
      type: ImageChange
    - imageChangeParams:
        automatic: true
        containerNames:
          - {{ .Values.containers.shibd.name | quote }}
        from:
          kind: ImageStreamTag
          name: {{ .Values.containers.shibd.image_name | quote }}
          namespace: {{ .Values.namespace | quote }}
      type: ImageChange
    - type: ConfigChange
