ServerRoot "/etc/httpd"
Listen 8080

StartServers 1
MinSpareServers 1
# MaxSpareServers has to be at least `MinSpareServers` + 1
# https://httpd.apache.org/docs/2.4/mod/prefork.html#maxspareservers
MaxSpareServers 2
MaxRequestWorkers 16

Include conf.modules.d/*.conf
LoadModule wsgi_module modules/mod_wsgi.so
LoadModule mod_shib /usr/lib64/shibboleth/mod_shib_24.so

Include /etc/httpd/conf.d/shib_httpd.conf

User apache
Group apache

#Default vhost that covers cases where the route still points to the service but no Vhost is defined
<VirtualHost *:8080>
    ServerName generic
    ServerAdmin root@localhost
    DocumentRoot /var/www/html
</VirtualHost>

IncludeOptional conf.d/vhost/*.conf

LogFormat "[%D] [server %V] %{X-Forwarded-For}i %h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %p %D" combined
# LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
LogLevel warn
ErrorLog "|/usr/sbin/cronolog /var/log/httpd/%Y/%m/%Y-%m-%d-error.log"
CustomLog "|/usr/sbin/cronolog /var/log/httpd/%Y/%m/%Y-%m-%d-access.log" combined

ErrorDocument 401 /var/www/html/401.html
ErrorDocument 403 /var/www/html/403.html
ErrorDocument 404 /var/www/html/404.html
ErrorDocument 405 /var/www/html/405.html
ErrorDocument 406 /var/www/html/406.html
ErrorDocument 412 /var/www/html/412.html
ErrorDocument 500 /var/www/html/500.html
ErrorDocument 501 /var/www/html/501.html
ErrorDocument 502 /var/www/html/502.html
ErrorDocument 503 /var/www/html/503.php

#Security Settings
ServerSignature Off
ServerTokens Prod
FileETag None

UseCanonicalName On


# <Directory "/var/www/cgi-bin">
#     AllowOverride None
#     # Order & Allow are necessary for wsgi to run
#     Order allow,deny
#     Allow from all
# </Directory>


<IfModule dir_module>
    DirectoryIndex index.php index.html
</IfModule>


<Files ".ht*">
    Require all denied
</Files>

# WSGIScriptAlias /cgi-bin/ /var/www/cgi-bin/

AddType application/x-httpd-php .php

<IfModule mime_module>
    TypesConfig /etc/mime.types
    AddType application/x-compress .Z
    AddType application/x-gzip .gz .tgz
    AddType application/x-httpd-php .php
    AddType application/x-httpd-php-source .phps
    AddType text/html .shtml
    AddOutputFilter INCLUDES .shtml
</IfModule>

#EnableMMAP off
EnableSendfile on
#ErrorLog /dev/stderr
#TransferLog /dev/stdout
#LogLevel debug
PidFile /usr/local/httpd/httpd.pid
ServerName https://${HOSTNAME_FQDN}
